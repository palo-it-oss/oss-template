# Palo IT's Contributing Guide

Hi! We are really excited that you are interested in contributing to our open source project. Before submitting your contribution, please make sure to take a moment and read through the following guidelines:

- [Code of Conduct](CODE_OF_CONDUCT.md)
- [Issue Reporting Guidelines](#issue-reporting-guidelines)
- [Merge Request Guidelines](#merge-request-guidelines)
- [Commit Message Guidelines](#commit-message-guidelines)
- [Testing Tips](#testing-tips)
- [Project Structure](#projet-structure)
- [Development Setup](#development-setup)

## Issue Reporting Guidelines

To maintain an effective bugfix workflow and make sure issues will be solved in a timely manner we kindly ask reporters to follow some simple guidelines.

Before creating an issue, please do the following :

⚠️ <LINK_TO_BE_CHANGED>

- Check the [existing issues](LINK_TO_ISSUES_TO_BE_ADDED) to make sure you are not duplicating one
- Verify, that the issue you are about to report does not relate to any of the dependencies used in the project

If you are sure that the problem you are experiencing is caused by a bug, file a new issue in a GitLab issue tracker following the recommendations below.

### Issue Template

Please use the appropriate Issue Template.

For example, use the `Bug` Issue Template for reporting a bug inside the platform.

### Issue Title

Title is a vital part of bug report for developer and triager to quickly identify a unique issue. A well written title should contain a clear, brief explanation of the issue, making emphasis on the most important points.

Good example would be:

> Unable to create a new demand as the verification fail.

Unclear example:

> Can't post a demand.

### Issue Description

For every new Issue, you should follow this kind of pattern :

- Summary
- Steps to reproduce
- Example Project
- What is the current behavior
- What is the expected behavior
- Relevant logs and/or screenshots
- Possible fixes

## Merge Request Guidelines

- The `main` branch is just a snapshot of the latest stable release. All development should be done in dedicated branches.  
  **Do not submit MRs/PRs against the `main` branch.**

- Checkout a topic branch from the relevant branch, e.g. `dev`, and merge back against that branch.

- It's OK to have multiple small commits as you work on the MR/PR - They will be squashed before merging.

- If adding a new feature:

  - Add accompanying test case.
  - Provide a convincing reason to add this feature. **Ideally, you should open a suggestion issue first and have it approved before working on it.**

- If fixing bug:
  - If you are resolving a special issue, add `(fix #xxxx[,#xxxx])` (#xxxx is the issue id) in your MR/PR title for a better release log, e.g. `update entities encoding/decoding (fix #3899)`.
  - Provide a detailed description of the bug in the MR/PR. Live demo preferred.
  - Add appropriate test coverage if applicable.

## Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted. This leads to **more readable messages** that are easy to follow when looking through the **project history**.

### Commit Message Format

Each commit message consists of a **header**, a **body** and a **footer**. The header has a special format that includes a **type**, a **scope** and a **subject**:

```text
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier to read on GitLab/Github as well as in various git tools.

#### Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

#### Type

Must be one of the following:

- **feat**: A new feature
- **fix**: A bug fix
- **docs**: Documentation only changes
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **perf**: A code change that improves performance
- **test**: Adding missing tests
- **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation

#### Scope

The scope could be anything specifying place of the commit change.

The valid scopes are:

- `api` for any changes concerning the backend
- `front` for any changes concerning the frontend
- `db` for any changes concerning the database
- `deploy` for any changes concerning the deployment
- ⚠️ <OTHERS_TO_BE_ADDED>

#### Subject

The subject contains succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- capitalize first letter
- no dot (.) at the end
- should not be more than 50 characters

#### Body

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes". The body should include the motivation for the change and contrast this with previous behavior.
Wrap it at 72 characters max.

#### Footer

The footer should contain any information about **Breaking Changes** and is also the place to reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

## Testing Tips

Tests will be part of the continous delivery, stay tuned.

## Project Structure

⚠️ <DESCRIBE_PROJECT_STRUCTURE>

## Development Setup

### Requirements

⚠️ <ADD_PROJECT_REQUIREMENTS (example for nodejs)>

Please install the following (version check 08.2023) :

The examples of command line are for a GNU/Linux Debian based OS.

- [NVM](https://github.com/nvm-sh/nvm)

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash
```

- [NodeJS](https://nodejs.org/) (in use : v18 LTS)

```
nvm install v18
```

⚠️ <AND SO ON (docker, docker compose, docker rootless mode, etc)>

### Additional tools

- [LazyDocker](https://github.com/jesseduffield/lazydocker)
  -> A simple terminal UI for both docker and docker-compose

### Installation

⚠️ <ADD_PROJECT_INSTALLATION_DETAILS>
